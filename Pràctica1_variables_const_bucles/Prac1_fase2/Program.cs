﻿using System;

namespace Pract1_Fase2
{
    class Program
    {
        static void Main(string[] args)
        {
            const int any = 1948;
            const int traspas = 4;
            int anyNeixe = 1974;
            int anysTras = 0;
            anysTras = (anyNeixe - any) / traspas;
            Console.WriteLine("\n\tfEls anys de traspàs són " + anysTras);
            Console.ReadKey();
        }
    }
}

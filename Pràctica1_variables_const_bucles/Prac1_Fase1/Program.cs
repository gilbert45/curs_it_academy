﻿using System;

namespace Prac1_Fase1
{
    class Program
    {
        static void Main(string[] args)
        {
            string nom = "Gilbert";
            string cognom1 = "Viader";
            string cognom2 = "Sauret";
            int dia = 7;
            int mes = 10;
            int any = 2019;
            Console.WriteLine("\n\t"+cognom1 + " " + cognom2 + ", " + nom);
            Console.WriteLine("\t"+dia + " / " + mes + " / " + any);
            Console.ReadKey();
        }
    }
}

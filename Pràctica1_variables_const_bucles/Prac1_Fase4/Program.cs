﻿using System;

namespace Prac1_Fase4
{
    class Program
    {
        static void Main(string[] args)
        {
            string nom = "Gilbert";
            string cognom1 = "Viader";
            string cognom2 = "Sauret";
            int dia = 7;
            int mes = 10;
            int any = 2019;
            //const int any = 1948;
            //const int traspas = 4;
            int anyinici = 1948;
            int anyFinal = 1974;
            //int anysTras = 0;
            bool hoes = false;
            string nomComplert = nom + " " + cognom1 + " " + cognom2;
            string dataNeixement = dia + " / " + mes + " / " + any;
            if (anyFinal - anyinici % 4 == 0)
            {
                hoes = true;
            }
            else
            {
                hoes = false;
            }
            string veritatAny = " és un any de trspàs ";
            string falsAny = " no és cap any de traspàs";
            Console.WriteLine(nomComplert);
            Console.WriteLine(dataNeixement);
            if (hoes == true)
            {
                Console.WriteLine(anyFinal + veritatAny);
            }
            else
            {
                Console.WriteLine(anyFinal + falsAny);
            }
            Console.ReadKey();

        }
    }
}

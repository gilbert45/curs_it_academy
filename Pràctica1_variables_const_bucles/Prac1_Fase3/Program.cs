﻿using System;

namespace Practica1_Fase3
{
    class Program
    {
        static void Main(string[] args)
        {
            int anyinici = 1948;
            int anyFinal = 1974;
            int anysTras = 0;
            bool hoes = false;

            for (int i=anyinici; i<=anyFinal; i += 4)
            {
                anysTras += 1;
                Console.Write("\n\tL'any {0} de traspàs és el {1}",anysTras, i);
            }
            if ((anyFinal - anyinici) % 4 == 0)
            {
                hoes = true;
            }
            else
            {
                hoes = false;
            }

            if (hoes == true)
            {
                string veritatAny = " és un any de traspàs ";
                Console.WriteLine("\n\t"+anyFinal + veritatAny);
            }
            else
            {
                string falsAny = " no és cap any de traspàs";
                Console.WriteLine("\n\t"+anyFinal + falsAny);
            }
            Console.ReadKey();

        }
    }
}

// codi sql per la BBDD Xarxasocial
// crearem els camps de la primera taula anomenada membres
ALTER TABLE `membres` ADD PRIMARY KEY(`id_membre`);
ALTER TABLE `membres` CHANGE `id_membre` `id_membre` INT(8) NOT NULL AUTO_INCREMENT;
ALTER TABLE `membres` CHANGE `xnom` `xnom` INT(15) NOT NULL DEFAULT '0';
ALTER TABLE `membres` CHANGE `xnom` `xnom` VARCHAR(15) NOT NULL;
ALTER TABLE `membres` CHANGE `xemail` `xemail` VARCHAR(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `membres` CHANGE `id_Rfoto` `id_Rfoto` INT(8) NOT NULL DEFAULT '0';
// crearem els camps de la segona taula anomenada amic i relacionada amb
// la taula membres
ALTER TABLE `amic` ADD PRIMARY KEY(`id_amic`);
ALTER TABLE `amic` CHANGE `id_amic` `id_amic` INT(8) NOT NULL AUTO_INCREMENT;
// crearem els camps de la tercera taula anomenada fotografi i relacionada amb
// la taula membres
ALTER TABLE `fotografia` ADD `nomFotografia` VARCHAR(25) NOT NULL AFTER `id_foto`;
ALTER TABLE `fotografia` ADD PRIMARY KEY(`id_foto`);
ALTER TABLE `fotografia` CHANGE `id_foto` `id_foto` INT(8) NOT NULL AUTO_INCREMENT;
// construir la relació entre la taula Membres amb la taula amic
ALTER TABLE `membres` ADD CONSTRAINT `keyForest` FOREIGN KEY (`id_Ramic`) REFERENCES `amic`(`id_amic`) ON DELETE CASCADE ON UPDATE CASCADE;
// construir la relació entre la taula Membres amb la taula fotografia
ALTER TABLE `membres` ADD CONSTRAINT `keyForest2` FOREIGN KEY (`id_Rfoto`) REFERENCES `fotografia`(`id_foto`) ON DELETE CASCADE ON UPDATE CASCADE;

// codi SQL per la basses de dades "ulleres"
// creació de la primera taula anomenada "ulleres"
ALTER TABLE `ulleres` ADD `marca` VARCHAR(15) NOT NULL AFTER `id_ulleres`;
ALTER TABLE `ulleres` ADD `graduacio` FLOAT(4) NOT NULL DEFAULT '5.5' AFTER `marca`;
ALTER TABLE `ulleres` ADD `tipusMontura` SET('Flotant','Pasta','Metaliques','') NOT NULL AFTER `graduacio`;
ALTER TABLE `ulleres` ADD `colorMontura` SET('Blanques','Negres','Marrons','Grisses','Daurades','Platejades','') NOT NULL AFTER `tipusMontura`;
ALTER TABLE `ulleres` ADD `colorVidre1` SET('Transparents','Opaques','Fosques','') NOT NULL AFTER `colorMontura`;
ALTER TABLE `ulleres` ADD `colorVidre2` SET('Transparents','Opaques','Fosques','') NOT NULL AFTER `colorVidre1`;
ALTER TABLE `ulleres` ADD `id_Fprovedor` INT(8) NOT NULL DEFAULT '0' AFTER `colorVidre2`;
ALTER TABLE `ulleres` ADD `id_Fempleat` INT(8) NOT NULL DEFAULT '0' AFTER `id_Fprovedor`;
ALTER TABLE `ulleres` ADD `id_Fclient` INT(8) NOT NULL DEFAULT '0' AFTER `id_Fempleat`;
ALTER TABLE `ulleres` ADD PRIMARY KEY(`id_ulleres`);
ALTER TABLE `ulleres` CHANGE `id_ulleres` `id_ulleres` INT(8) NOT NULL AUTO_INCREMENT;
ALTER TABLE `ulleres` CHANGE `marca` `marca` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL;
// creació de la segona taula anomenada "proveidor"
ALTER TABLE `proveidor` ADD `nom` VARCHAR(15) NOT NULL AFTER `id_proveidor`;
ALTER TABLE `proveidor` ADD `direccio` VARCHAR(35) NOT NULL DEFAULT 'carrer / placa / Avg' AFTER `nom`;
ALTER TABLE `proveidor` ADD `CP` INT(5) UNSIGNED ZEROFILL NOT NULL DEFAULT '08000' AFTER `direccio`;
ALTER TABLE `proveidor` ADD `ciutat` SET('Barcelona','Paris','Lisboa','Roma','Madrid','') NOT NULL AFTER `CP`;
ALTER TABLE `proveidor` ADD `pais` SET('Catalunya','França','Itàlia','Portugal','Espanya','') NOT NULL AFTER `ciutat`;
ALTER TABLE `proveidor` ADD `mobil` INT(9) NOT NULL DEFAULT '600000000' AFTER `pais`;
ALTER TABLE `proveidor` ADD `nif` INT(9) NOT NULL DEFAULT '111111111' AFTER `Fax`;
ALTER TABLE `proveidor` ADD PRIMARY KEY(`id_proveidor`);
ALTER TABLE `proveidor` CHANGE `id_proveidor` `id_proveidor` INT(8) NOT NULL AUTO_INCREMENT;
// creació de la tercera taula anomenada "empleat"
ALTER TABLE `empleat` ADD `nom` VARCHAR(15) NOT NULL DEFAULT 'empleat' AFTER `id_empleat`;
ALTER TABLE `empleat` ADD `mobil` INT(9) NOT NULL DEFAULT '600000000' AFTER `cp`;
ALTER TABLE `empleat` ADD `nif` INT(9) NOT NULL DEFAULT '111111111' AFTER `mobil`;
ALTER TABLE `empleat` ADD PRIMARY KEY(`id_empleat`);
ALTER TABLE `empleat` CHANGE `id_empleat` `id_empleat` INT(8) NOT NULL AUTO_INCREMENT;
// creació de la quarta taula anomenada "client"
ALTER TABLE `client` ADD `nom` VARCHAR(15) NOT NULL DEFAULT 'client' AFTER `id_client`;
ALTER TABLE `client` ADD `cp` INT(5) UNSIGNED ZEROFILL NOT NULL DEFAULT '08001' AFTER `nom`;
ALTER TABLE `client` ADD `mobil` INT(9) NOT NULL DEFAULT '600000000' AFTER `cp`;
ALTER TABLE `client` ADD `nif` INT(9) NOT NULL DEFAULT '111111111' AFTER `mobil`;
ALTER TABLE `client` ADD PRIMARY KEY(`id_client`);
ALTER TABLE `client` CHANGE `id_client` `id_client` INT(8) NOT NULL AUTO_INCREMENT;
// relació de la taula ulleres amb la taula proveidor
ALTER TABLE `ulleres` ADD  CONSTRAINT `keyForest` FOREIGN KEY (`id_Fprovedor`) REFERENCES `proveidor`(`id_proveidor`) ON DELETE CASCADE ON UPDATE CASCADE
// relació de la taula ulleres amb la taula empleat
ALTER TABLE `ulleres` ADD CONSTRAINT `keyForest2` FOREIGN KEY (`id_Fempleat`) REFERENCES `empleat`(`id_empleat`) ON DELETE CASCADE ON UPDATE CASCADE;
// relació de la taula ulleres amb la taula client
ALTER TABLE `ulleres` ADD CONSTRAINT `keyforest3` FOREIGN KEY (`id_Fclient`) REFERENCES `client`(`id_client`) ON DELETE CASCADE ON UPDATE CASCADE;

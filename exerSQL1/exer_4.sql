// codi sql de la bbdd de la bibliotece
// primera taula anomenada Llibre
ALTER TABLE `llibre` ADD PRIMARY KEY(`id_llibre`);
ALTER TABLE `llibre` CHANGE `id_llibre` `id_llibre` INT(8) NOT NULL 
AUTO_INCREMENT;
ALTER TABLE `llibre` CHANGE `id_Ruser` `id_Ruser` INT(8) NOT NULL 
DEFAULT '0';
ALTER TABLE `llibre` CHANGE `id_Rautor` `id_Rautor` INT(8) NOT NULL 
DEFAULT '0';
ALTER TABLE `llibre` CHANGE `autor` `autor` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `llibre` CHANGE `preu` `preu` MEDIUMINT(4) NOT NULL DEFAULT '0';
ALTER TABLE `llibre` CHANGE `stock` `stock` INT(8) NOT NULL DEFAULT '0';
ALTER TABLE `llibre` CHANGE `llibre` `llibre` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'name of book';
// segona taula anomenada autor relacionada amb taula Llibre
ALTER TABLE `autor` ADD PRIMARY KEY(`int id_autor`);
ALTER TABLE `autor` CHANGE `int id_autor` `int id_autor` INT(8) NOT NULL AUTO_INCREMENT;
ALTER TABLE `autor` CHANGE `autor` `autor` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `autor` CHANGE `int id_autor` `id_autor` INT(8) NOT NULL AUTO_INCREMENT;
// tercera taula anomenada Usuari relacionada també amb taula Llibre
ALTER TABLE `usuari` ADD PRIMARY KEY(`id_usuari`);
ALTER TABLE `usuari` CHANGE `id_usuari` `id_usuari` INT(8) NOT NULL AUTO_INCREMENT;
ALTER TABLE `usuari` CHANGE `nom` `nom` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'name of user';
ALTER TABLE `usuari` CHANGE `email` `email` VARCHAR(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `usuari` CHANGE `contrasenya` `contrasenya` VARCHAR(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'xxx-xxx-xxxx';
// quarta taula anomenada Factura relacionada amb la taula Usuari
ALTER TABLE `factura` ADD `id_factura` INT(8) NOT NULL FIRST;
ALTER TABLE `factura` CHANGE `codiFactura` `codiFactura` VARCHAR(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `factura` ADD PRIMARY KEY(`id_factura`);
ALTER TABLE `factura` CHANGE `id_factura` `id_factura` INT(8) NOT NULL AUTO_INCREMENT;
// construir la relació de la taula Llibre amb la taula autor
ALTER TABLE `llibre` ADD CONSTRAINT `keyForest` FOREIGN KEY (`id_Rautor`) REFERENCES `autor`(`int id_autor`) ON DELETE CASCADE ON UPDATE CASCADE;
// construir la relació de la taula Llibre amb la taula Usuari
ALTER TABLE `llibre` ADD CONSTRAINT `keyForest2` FOREIGN KEY (`id_Ruser`) REFERENCES `usuari`(`id_usuari`) ON DELETE CASCADE ON UPDATE CASCADE;
// construir la relació de la taula usuari amb la taula Factura
ALTER TABLE `usuari` ADD CONSTRAINT `keyForest3` FOREIGN KEY (`id_Rfactura`) REFERENCES `factura`(`id_factura`) ON DELETE CASCADE ON UPDATE CASCADE;
// insercions de registres per cada taula
INSERT INTO `autor` (`id_autor`, `autor`, `edicions`) VALUES (NULL, 'Eileen Bringes', 'four book edition, El tiempo de los enigmas, cosas de casas, todo el saber,\r\ncomprar con moderacion.');
INSERT INTO `factura` (`id_factura`, `codiFactura`) VALUES (NULL, '2323-455-ASA');
INSERT INTO `usuari` (`id_usuari`, `nom`, `email`, `contrasenya`, `id_Rfactura`) VALUES (NULL, 'Gilbert', 'dfs@gmail.com', '123-232', '1');
INSERT INTO `llibre` (`id_llibre`, `llibre`, `stock`, `preu`, `autor`, `id_Rautor`, `id_Ruser`) VALUES (NULL, 'Cosas de casas', '10000', '24', 'Eileen Bringes', '1', '1');





﻿using System;

namespace Prac3_Fase3
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena;
            string[] arrayCiutat = new string[6];
            // inicialització del nou array on serà modificats alguns caràcters, array finit de 6 posicions
            string[] arrayCiutatModificades = new string[6];
            for (int i = 0; i < arrayCiutat.Length; i++)
            {
                int j = i + 1;
                Console.Write("\n\tIntroduir la  ciutat {0}? ", j);
                cadena = Console.ReadLine();
                // assignem a l'array el nom de cada ciutat per posició d'aquest
                // tot seguit amb el mètode Replace(char, char) modifiquem les ciutats en el nou array
                arrayCiutat[i] = cadena;
                arrayCiutatModificades[i] = arrayCiutat[i].Replace("a", "4");
            }
            // mètode Sort on seran les posicions de l'array de string ordenat per ordre alfabètic
            Array.Sort(arrayCiutatModificades);
            Console.Write("\n\tLes 6 ciutats ordenades alfabeticament són: ");
            for (int i = 0; i < arrayCiutatModificades.Length; i++)
            {
                //int j = i + 1;
                if (i != 5 && i != 4) Console.Write(" {0},", arrayCiutatModificades[i]);
                else
                {
                    if (i == 4) Console.Write(" {0}", arrayCiutatModificades[i]);
                    else Console.Write(" i {0}.", arrayCiutatModificades[i]);
                }
            }
            ConsoleKeyInfo clau = Console.ReadKey();
        }
    }
}

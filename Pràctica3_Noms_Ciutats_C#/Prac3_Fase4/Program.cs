﻿using System;
using System.Linq;

namespace Prac3_Fase4
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena1 = "";
            string cadena2 = "";
            string cadena3 = "";
            string cadena4 = "";
            string cadena5 = "";
            string cadena6 = ""; 

            for (int i = 0; i < 6; i++)
            {
                Console.Write("\n\tIntroduir la ciutat {0}: ", i+1);
                if (i == 0) cadena1 = Console.ReadLine();
                if (i == 1) cadena2 = Console.ReadLine();
                if (i == 2) cadena3 = Console.ReadLine();
                if (i == 3) cadena4 = Console.ReadLine();
                if (i == 4) cadena5 = Console.ReadLine();
                if (i == 5) cadena6 = Console.ReadLine();
            }
            char[] ciutat1 = new char[cadena1.Length];
            for (int l = 0; l < cadena1.Length; l++)
            {
                ciutat1[l] = cadena1[l];
            }
 
            char[] ciutat2 = new char[cadena2.Length];
            for (int l = 0; l < cadena2.Length; l++)
            {
                ciutat2[l] = cadena2[l];
            }

            char[] ciutat3 = new char[cadena3.Length];
            for (int l = 0; l < cadena3.Length; l++)
            {
                ciutat3[l] = cadena3[l];
            }

            char[] ciutat4 = new char[cadena4.Length];
            for (int l = 0; l < cadena4.Length; l++)
            {
                ciutat4[l] = cadena4[l];
            }
            
            char[] ciutat5 = new char[cadena5.Length];
            for (int l = 0; l < cadena5.Length; l++)
            {
                ciutat5[l] = cadena5[l];
            }
            
            char[] ciutat6 = new char[cadena6.Length];
            for (int l = 0; l < cadena6.Length; l++)
            {
                ciutat6[l] = cadena6[l];
            }

            Console.Write("\n\n\t");

            Console.Write(ciutat1.Reverse().ToArray());
            Console.Write(", ");
            Console.Write(ciutat2.Reverse().ToArray());
            Console.Write(", ");
            Console.Write(ciutat3.Reverse().ToArray());
            Console.Write(", ");
            Console.Write(ciutat4.Reverse().ToArray());
            Console.Write(", ");
            Console.Write(ciutat5.Reverse().ToArray());
            Console.Write(" i ");
            Console.Write(ciutat6.Reverse().ToArray());
            Console.Write(".");
            Console.ReadKey();
        }
    }
}

﻿using System;

namespace Prac3_Fase1
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena1 = "";  // assignació de 6 variables string
            string cadena2 = "";
            string cadena3 = "";
            string cadena4 = "";
            string cadena5 = "";
            string cadena6 = "";
            for (int i = 0; i < 6; i++)
            {
                var j = i + 1; // posició real de la ciutat
                Console.Write("\n\tIntroduir la  ciutat {0}? ", j);
                if (i == 0) cadena1 = Console.ReadLine(); // introduir els 6 noves ciutats en els 6 string
                if (i == 1) cadena2 = Console.ReadLine();
                if (i == 2) cadena3 = Console.ReadLine();
                if (i == 3) cadena4 = Console.ReadLine();
                if (i == 4) cadena5 = Console.ReadLine();
                if (i == 5) cadena6 = Console.ReadLine();
            }
            Console.WriteLine("\n\tLes ciutats són: {0}, {1}, {2}, {3}, {4} i {5}", cadena1, cadena2, cadena3, cadena4, cadena5, cadena6);
            // sortida de dades per pantalla
            ConsoleKeyInfo clau = Console.ReadKey();
        }
    }
}

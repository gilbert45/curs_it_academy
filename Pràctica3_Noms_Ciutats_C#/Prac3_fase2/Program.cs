﻿using System;

namespace Prac3_Fase2
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena; // string d'entrada de teclat
            // crearem un array de string definit de 6 posicions per cada una de les ciutats
            string[] arrayCiutat = { " ", " ", " ", " ", " ", " " };
            for (int i = 0; i < 6; i++)
            {
                var j = i + 1;
                Console.Write("\n\tIntroduir la  ciutat {0}? ", j);
                cadena = Console.ReadLine();
                arrayCiutat[i] = cadena; // assignació de les ciutats per cada posició de l'array.
            }
            Array.Sort(arrayCiutat); // ordenació alfabèticament de les ciutats en l'array de strings.
            Console.Write("\n\tLes 6 ciutats ordenades alfabeticament són: ");
            for (int i = 0; i < 6; i++)
            {
                // modificarem les sortides última i penúltima per acoseguir la visualització desitjada
                if (i!=5 && i!=4) Console.Write(" {0},", arrayCiutat[i]);
                else
                {
                    if(i==4) Console.Write(" {0}", arrayCiutat[i]);
                    else Console.Write(" i {0}.", arrayCiutat[i]);
                }
            }// sortida de les ciutats per pantalla
            
            ConsoleKeyInfo clau = Console.ReadKey();
        }
    }
}

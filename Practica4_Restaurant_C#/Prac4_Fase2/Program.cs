﻿using System;
using System.Collections.Generic;

namespace Prac4_Fase3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\n\t\tPRÀCTICA 4 : FASE 3");

            int billet5 = 5;
            int billet10 = 10;
            int billet20 = 20;
            int billet50 = 50;
            int billet100 = 100;
            int billet200 = 200;
            int billet500 = 500;

            double total = 0;

            string[] arrayPlats = new string[5];
            string plat = "";
            double[] arrayPreus = new double[5];
            string strpreu = "";
            double preu = 0;
            for (int i = 0; i < arrayPlats.Length; i++)
            {
                Console.Write("\t\tIntroduir el nom del plat {0}: ", i + 1);
                plat = Console.ReadLine();
                arrayPlats[i] = plat;
                do {
                    Console.Write("\t\tIntroduir el preu del plat {0} (entre 4 i 14 euros) : ", i + 1);
                    strpreu = Console.ReadLine();
                    preu = 15;
                    try
                    {
                        preu = Double.Parse(strpreu);
                    }
                    catch (Exception e)
                    {    
                    }     
                } while (preu < 4 || preu > 14);
                arrayPreus[i] = preu;
            }

            Console.WriteLine("\n\t\t\t   MENÚ A LA CARTA");
            for (int i = 0; i < arrayPlats.Length; i++)
            {
                Console.WriteLine("\t\t\t{0}: {1}......{2} euros", i + 1, arrayPlats[i], arrayPreus[i]);
            }
            Console.WriteLine();
            string menjar = "";
            int seguir = 2;
            List<string> llista = new List<string>();
            while (true)
            {
                Console.Write("\t\tQue vols menjar ? ");
                menjar = Console.ReadLine();
                llista.Add(menjar);
                Console.WriteLine();
                do
                {
                    Console.Write("\t\tVols un altre plat [1:Si / 0:No]? ");
                    menjar = Console.ReadLine();
                } while (menjar != "0" && menjar != "1");
            seguir = Int16.Parse(menjar);
            if (seguir == 0) break;
            }
        }
    }
}

﻿using System;
using System.Linq;

namespace Prac5_Fase4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\t\tLINQ: Pràctica 5 : Fase 4");
            // en aquesta fase farem servir un array amb noms: (David, Sergio, Maria, Laura, Oscar, Julia, Oriol)
            // Creeu un array amb els noms de l'enunciat. (0,5 punts)
            string[] array = new string[7] { "David", "Sergio", "Maria", "Laura", "Oscar", "Julia", "Oriol" };
            // Treurem tots els noms per pantalla
            Console.Write("\n\tEls noms de l'array són: ");
            for (int i=0; i<array.Length; i++)
            {
                Console.Write("{0} ", array[i]);
            }
            // Fent una consulta linq mostreu per consola els noms que comencin amb la lletra "O" (1 punt).
            var consulta1 = from nom in array where (nom[0] == 'O') select nom;
            Console.Write("\n\n\tEls noms amb la lletra 'O' són: ");
            foreach (string nom in consulta1)
            {
                Console.Write("{0} ", nom);
            }
            // Fent servir consultes linq mostreu per consola quins noms tenen més de 6 lletres (1 punt).
            var consulta2 = from nom in array where (nom.Count() >= 6) select nom;
            Console.Write("\n\n\tEls noms que tenen 6 lletres o més són: ");
            foreach (string nom in consulta2)
            {
                Console.Write("{0}", nom);
            }
            // Fen servir consultes linq mostreu per consola els noms de l'array ordenats de manera descendent (1 punt).
            var consulta3 = from nom in array orderby nom select nom;
            Console.Write("\n\n\tTots els noms ordenats alfabet: ");
            foreach (string nom in consulta3)
            {
                Console.Write("{0} ", nom);
            }
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Linq;

namespace Prac5_Fase3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\t\tLINQ: Pràctica 5 : Fase 3");
            int[] array = new int[15] { 2, 6, 8, 4, 5, 5, 9, 2, 1, 8, 7, 5, 9, 6, 4 };
            // imprimir en pantalla l'array
            Console.Write("\n\tEls nombres de l'array són: (");
            for (int i=0; i < array.Length; i++)
            {
                if (i == array.Length - 1) Console.Write("{0} ", array[i]);
                else Console.Write("{0}, ", array[i]);
            }
            // Fem una consulta Linq guardeu en un array quins número són més grans que 5 i en un altre 
            // array quins són més petits (1,5 punts)
            var majors = from num in array where (num >= 5) select num;
            var minims = from num in array where (num < 5) select num;
            // Mostreu per consola els dos array (1 punt)
            Console.Write(")\n\n\tL'array dels nombre més gran de 5 : (");
            foreach (int num in majors)
            {
                if (num == majors.Last()) Console.Write("{0} ", num);
                else Console.Write("{0}, ", num);
            }
            Console.Write(")\n\n\tL'array dels nombre més petits de 5 : (");
            foreach (int num in minims)
            {
                if (num == minims.Last()) Console.Write("{0} ", num);
                else Console.Write("{0}, ", num);
            }
            Console.WriteLine(")");
            Console.ReadKey();
        }
    }
}

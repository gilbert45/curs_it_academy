﻿using System;
using System.Linq;

namespace Prac5_Fase1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\t\tLINQ: Pràctica 5 : Fase 1");
            // Crear una array amb els números donats al principi de la pràctica (0,5 punts)
            int[] array = new int[15] { 2, 6, 8, 4, 5, 5, 9, 2, 1, 8, 7, 5, 9, 6, 4 };
            // Imprimir per pantalla els valors de l'array de nombres sencers.
            Console.Write("\n\tL'array és: ");
            for (int i=0; i < array.Length; i++)
            {
                if (i == array.Length - 1) Console.Write("{0} ", array[i]);
                else Console.Write("{0}, ", array[i]);
            }
            // Fent una consulta LINQ guardeu en un array tots els numeros parells (1 punt).
            var parell = from num in array where (num % 2) == 0 select num;
            // Mostreu l'array de números parells per consulta.(0,5 punts).
            Console.Write("\n\tEls nombres parells són: ");
            
            foreach (int num in parell)
            {
              
                if (num == parell.Last()) Console.Write("{0} ", num);
                else Console.Write("{0}, ", num);
            }
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Linq;

namespace Pract5_Fase2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\t\tLINQ: Pràctica 5 : Fase 2");
            int[] array = new int[15] { 2, 6, 8, 4, 5, 5, 9, 2, 1, 8, 7, 5, 9, 6, 4 };
            //var parell = from num in array where (num % 2) == 0 select num;
            // calculeu a partir de l'array anterior la nota mitjana, la màxima i la mínima.(1 punt)
            var mitjana = array.Average();
            var minim = array.Min();
            var maxim = array.Max();
            // Mostreu per consola els resultats (1 punt)
            Console.Write("\n\tEl càlcul de la mitjana, el mínim i el màxim de l'array: (");
            foreach (int num in array)
            {
                if (num == array.Last()) Console.Write("{0} ", num);
                else Console.Write("{0}, ", num);
            }
            Console.WriteLine(")\n\n\t serà de {0} la mitjana, de {1} el mínim, i de {2} el màxim.", mitjana, minim, maxim);
            Console.ReadKey();
        }
    }
}

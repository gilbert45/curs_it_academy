﻿using System;
using System.Collections.Generic;

namespace p2_Fase3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<char> llista = new List<char>();
            llista.Add('g');
            llista.Add('i');
            llista.Add('l');
            llista.Add('b');
            llista.Add('e');
            llista.Add('r');
            llista.Add('g');

            Dictionary<char, int> diccionari = new Dictionary<char, int>();

            for (int i = 0; i < llista.Count; i++)
            {
                char exten = llista[i];
                if (diccionari.ContainsKey(exten))
                    diccionari[exten]++;
                else
                    diccionari.Add(exten, 1);

            }
            Console.WriteLine("\n\t CONTADOR DE LLETRES D'UNA LLISTA  I AFEGIR-LES EN UN DICCIONARI");
            foreach (var lletra in diccionari)
            {
                Console.WriteLine($"\n\t\t\tLa lletra {lletra.Key} amb {lletra.Value} vegades.");
            }
            Console.ReadKey();

        }


    }
}
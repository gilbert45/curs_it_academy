﻿using System;

namespace Practica2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\tARRAY DE CARÀCTERS\n");
            char[] array = { 'g', 'i', 'l', 'b', 'e', 'r', 't'};
            for (int i = 0; i < array.Length; i++)
            {
                int j = i + 1;
                Console.WriteLine("\t "+j+" : "+ array[i]);
            }
            Console.ReadKey();
        }
    }
}

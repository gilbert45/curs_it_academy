﻿using System;
using System.Collections.Generic;

namespace p2_Fase2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\tLLISTA DE CARÀCTERS\n");
            List<char> llista = new List<char>();
            llista.Add('g');
            llista.Add('i');
            llista.Add('l');
            llista.Add('b');
            llista.Add('e');
            llista.Add('r');
            llista.Add('t');
            llista.Add('9');

            int contador = 0;
            foreach (char lletra in llista)
            {
                contador++ ;
                Console.Write("\n\t" + contador + " : " + lletra);
                if (lletra == 'a' || lletra == 'e' || lletra == 'i' || lletra == 'o' || lletra == 'u')
                {
                    Console.WriteLine("\tVOCAL");
                }
                else
                {
                    if (lletra == '1' || lletra == '2' || lletra == '3' || lletra == '4' || lletra == '5' || lletra == '6' || lletra == '7' || lletra == '8' || lletra == '9' || lletra == '0')
                    {
                        Console.WriteLine(" Els noms de persona no contenen nombres");
                    }
                    else
                    {
                        Console.WriteLine("\tCONSONANT");
                    }
                }
            }
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace P2_Fase4
{
    class Program
    {
        static void Main(string[] args)
        {
            // llista del primer nom
            List<char> nom = new List<char>();
            nom.Add('G');
            nom.Add('i');
            nom.Add('l');
            nom.Add('b');
            nom.Add('e');
            nom.Add('r');
            nom.Add('t');
            // llista del segon nom
            List<char> cognom = new List<char>();
            cognom.Add('V');
            cognom.Add('i');
            cognom.Add('a');
            cognom.Add('d');
            cognom.Add('e');
            cognom.Add('r');
            // llista del nom complert separat per comes 
            List<char> nomComplert = new List<char>();
            for (int i = 0; i < nom.Count; i++)
            {
                nomComplert.Add(nom[i]);
                nomComplert.Add(',');
                
            }
            nomComplert.Add(' ');
            nomComplert.Add(',');
            for (int i = 0; i <  cognom.Count; i++)
            {
                nomComplert.Add(cognom[i]);
                if (i != cognom.Count - 1) nomComplert.Add(',');
            }

            Console.Write("\n\n\t\tFullName: [");
            foreach (var lletra in nomComplert)
            {
                if (lletra != ',') Console.Write("'");
                Console.Write(lletra);
                if (lletra != ',') Console.Write("'");
                else Console.Write(" ");
            }
            Console.Write("]");
            Console.ReadKey();
        }
    }
}
